Prerenderer Gem for Dinamodigitale Prerenderer

# Installation
```ruby
gem 'prerenderer_rails', bitbucket: "dinamodigitale/prerenderer_rails"
```

# Use
```ruby
config.middleware.use Rack::Prerenderer,
  apikey: "<<<YOUR-OWN-API-KEY>>>", # mandatory
  prerender_service_url: 'https://prerender-service-url', # mandatory
  replacement: ['http://localhost:3000', "http://your-production-domain.com"], # optional (development use)
  development_user_agent: "chrom" # optional (development use)
```

Enjoy and let the prerender force flow in your application! :-)
