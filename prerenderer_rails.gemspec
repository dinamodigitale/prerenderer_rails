# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "prerenderer_rails"
  spec.version       = "1.0.8"
  spec.authors       = ["Manuel Burato", "Motaz Abuthiab"]
  spec.email         = ["m.burato@dinamodigitale.it"]
  spec.description   = %q{Use prerenderer external service}
  spec.summary       = %q{Use prerenderer external service}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'rack', '>= 0'
  spec.add_dependency 'activesupport', '>= 0'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "webmock"
end
