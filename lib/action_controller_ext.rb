module ActionControllerBase
  module ClassMethods
    def self.extended(base)
      base.instance_variable_set("@prerender_actions", [])
    end

    def prerender *actions
      @prerender_actions = actions
    end

    def prerender_actions
      @prerender_actions || []
    end
  end
end

ActionController::Base.extend(ActionControllerBase::ClassMethods)
