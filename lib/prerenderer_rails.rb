require_relative "action_controller_ext.rb"
module Rack
  class Prerenderer
    attr_accessor :request
    require 'net/http'
    require 'active_support'

    def initialize(app, options={})
      @crawler_user_agents = [
        'googlebot',
        'yahoo',
        'bingbot',
        'baiduspider',
        'facebookexternalhit',
        'twitterbot',
        'rogerbot',
        'linkedinbot',
        'embedly',
        'bufferbot',
        'quora link preview',
        'showyoubot',
        'outbrain',
        'pinterest/0.',
        'developers.google.com/+/web/snippet',
        'www.google.com/webmasters/tools/richsnippets',
        'slackbot',
        'vkShare',
        'W3C_Validator',
        'redditbot',
        'Applebot',
        'WhatsApp',
        'flipboard',
        'tumblr',
        'bitlybot',
        'SkypeUriPreview',
        'nuzzel',
        'Discordbot',
        'Google Page Speed',
        'Qwantify'
      ]

      @extensions_to_ignore = [
        '.js',
        '.css',
        '.xml',
        '.less',
        '.png',
        '.jpg',
        '.jpeg',
        '.gif',
        '.pdf',
        '.svg',
        '.doc',
        '.txt',
        '.ico',
        '.rss',
        '.zip',
        '.mp3',
        '.rar',
        '.exe',
        '.wmv',
        '.doc',
        '.avi',
        '.ppt',
        '.mpg',
        '.mpeg',
        '.tif',
        '.wav',
        '.mov',
        '.psd',
        '.ai',
        '.xls',
        '.mp4',
        '.m4a',
        '.swf',
        '.dat',
        '.dmg',
        '.iso',
        '.flv',
        '.m4v',
        '.torrent'
      ]
      @options = options

      if !Rails.env.production? and @options[:development_user_agent].present?
        @crawler_user_agents << @options[:development_user_agent]
      end
      @options[:whitelist] = [@options[:whitelist]] if @options[:whitelist].is_a? String
      @options[:blacklist] = [@options[:blacklist]] if @options[:blacklist].is_a? String
      @extensions_to_ignore = @options[:extensions_to_ignore] if @options[:extensions_to_ignore]
      @crawler_user_agents = @options[:crawler_user_agents] if @options[:crawler_user_agents]
      @app = app
    end

    def call(env)
      dup._call(env)
    end

    def _call(env)
      @request = Rack::Request.new(env)

      @controller = nil
      @action = nil
      begin
        dup_env = env.dup
        dup_env['PATH_INFO'] = dup_env['PATH_INFO'][0..-2] if dup_env['PATH_INFO'][-1,1] == '/'
        dup_request = Rack::Request.new(dup_env)
        @app.router.recognize(dup_request) do |route, params|
          if params[:controller]
            @controller ||= "#{params[:controller].camelize}Controller".constantize rescue nil
            @action ||= params[:action] rescue nil
          end
        end
      rescue; end


      if should_show_prerendered_page(env)
        if @controller
          Rails.logger.info "[PRERENDERER] Processing: #{request.path} -> #{@controller.name}##{@action}"
        else
          Rails.logger.info "[PRERENDERER] Processing: #{request.path}"
        end

        cached_response = before_render(env)

        if cached_response
          return cached_response.finish
        end

        prerendered_response = get_prerendered_page_response(env)

        if prerendered_response
          response = build_rack_response_from_prerender(prerendered_response)
          if response
            after_render(env, prerendered_response)
            return response.finish
          end
        end
      else
        Rails.logger.info "[PRERENDERER] Skipping: #{request.path}"
      end

      @app.call(env)
    end


    def should_show_prerendered_page(env)
      user_agent = env['HTTP_USER_AGENT']
      Rails.logger.debug "[PRERENDERER] Checking user agent: #{user_agent}"
      buffer_agent = env['HTTP_X_BUFFERBOT']
      prerender_agent = env['HTTP_X_PRERENDER']
      is_requesting_prerendered_page = false

      return false if !user_agent
      return false if env['REQUEST_METHOD'] != 'GET'

      if @controller
        Rails.logger.info "[PRERENDERER] Check conditions: { controller: #{@controller.name}, prerender_actions: #{@controller.prerender_actions}, checking_action: #{@action.to_sym} }"
        if @controller.prerender_actions.include?(@action.to_sym)
          is_requesting_prerendered_page = true
        else
          return false
        end
      else
        Rails.logger.fatal("[PRERENDERER] No controller found for: #{@request.fullpath}")
        return false
      end

      #if it is a bot...show prerendered page
      if user_agent_match = @crawler_user_agents.any? { |crawler_user_agent| user_agent.downcase.include?(crawler_user_agent.downcase) }
        Rails.logger.debug "[PRERENDERER] found match with user agent #{user_agent_match}"
        is_requesting_prerendered_page = true
      else
        is_requesting_prerendered_page = false
      end

      is_requesting_prerendered_page = true if Rack::Utils.parse_query(request.query_string).has_key?('_escaped_fragment_')

      #if it is BufferBot...show prerendered page
      is_requesting_prerendered_page = true if buffer_agent

      #if it is Prerender...don't show prerendered page
      is_requesting_prerendered_page = false if prerender_agent

      #if it is a bot and is requesting a resource...dont prerender
      return false if @extensions_to_ignore.any? { |extension| request.fullpath.include? extension }

      # #if it is a bot and not requesting a resource and is not whitelisted...dont prerender
      # return false if @options[:whitelist].is_a?(Array) && @options[:whitelist].all? { |whitelisted| !Regexp.new(whitelisted).match(request.fullpath) }
      #
      # #if it is a bot and not requesting a resource and is blacklisted(url or referer)...dont prerender
      # if @options[:blacklist].is_a?(Array) && @options[:blacklist].any? { |blacklisted|
      #     blacklistedUrl = false
      #     blacklistedReferer = false
      #     regex = Regexp.new(blacklisted)
      #
      #     blacklistedUrl = !!regex.match(request.fullpath)
      #     blacklistedReferer = !!regex.match(request.referer) if request.referer
      #
      #     blacklistedUrl || blacklistedReferer
      #   }
      #   return false
      # end

      return is_requesting_prerendered_page
    end


    def get_prerendered_page_response(env)
      begin
        url = URI.parse(get_prerender_service_url)
        original_request = Rack::Request.new(env)

        request_url=original_request.url
        if @options[:replacement]
          request_url = request_url.gsub(@options[:replacement].first, @options[:replacement].second)
        end

        Rails.logger.info "[PRERENDERER] Sending request to server with { 'url': '#{request_url}' }"

        content = {
          url: request_url
        }

        headers = {
          "Content-Type" => "application/json"
        }

        headers['X-Api-Key'] = @options[:apikey]

        req = Net::HTTP::Post.new(url.request_uri, headers)
        req.body = content.to_json
        req.basic_auth(ENV['PRERENDER_USERNAME'], ENV['PRERENDER_PASSWORD']) if @options[:basic_auth]
        http = Net::HTTP.new(url.host, url.port)
        http.use_ssl = true if url.scheme == 'https'
        response = http.request(req)

        if response['Content-Encoding'] == 'gzip'
          response.body = ActiveSupport::Gzip.decompress(response.body)
          response['Content-Length'] = response.body.length
          response.delete('Content-Encoding')
        end


        response
      rescue Exception => e
        Rails.logger.fatal "[PRERENDERER] Exception making request to service: #{e.class.name} => #{e.message}"
        nil
      end
    end


    # def build_api_url(env)
    #   new_env = env
    #
    #   new_env["HTTPS"] = true and new_env["rack.url_scheme"] = "https" and new_env["SERVER_PORT"] = 443 if @options[:protocol] == "https"
    #   new_env["HTTPS"] = false and new_env["rack.url_scheme"] = "http" and new_env["SERVER_PORT"] = 80 if @options[:protocol] == "http" or @options[:protocol].nil?
    #
    #   url = Rack::Request.new(new_env).url
    #   prerender_url = get_prerender_service_url()
    #   forward_slash = prerender_url[-1, 1] == '/' ? '' : '/'
    #   "#{prerender_url}#{forward_slash}#{url}"
    # end


    def get_prerender_service_url
      @options[:prerender_service_url]
    end


    def build_rack_response_from_prerender(prerendered_response)
      data = JSON.parse(prerendered_response.body)

      if !data["body"].present?
        Rails.logger.warn "[PRERENDERER] Empty response from service [#{prerendered_response.code}] for url #{request.fullpath}"
        return nil
      end

      Rails.logger.info "[PRERENDERER] Receiving response from service [#{prerendered_response.code}] of #{data["body"].length} for url #{request.fullpath}"

      body = data["body"]

      data["headers"].delete "transfer-encoding"
      response = Rack::Response.new(body, prerendered_response.code, data["headers"])

      @options[:build_rack_response_from_prerender].call(response, prerendered_response) if @options[:build_rack_response_from_prerender]

      response
    end

    def before_render(env)
      return nil unless @options[:before_render]

      cached_render = @options[:before_render].call(env)

      if cached_render && cached_render.is_a?(String)
        Rack::Response.new(cached_render, 200, { 'Content-Type' => 'text/html; charset=utf-8' })
      elsif cached_render && cached_render.is_a?(Rack::Response)
        cached_render
      else
        nil
      end
    end


    def after_render(env, response)
      return true unless @options[:after_render]
      @options[:after_render].call(env, response)
    end
  end
end
